
**Cucumber with Java and Selenium Webdriver**
---------------------------------------------------------------------------


***Short introduction***

This is an example of maven - cucumber - jvm project based around http://www.orangehrm.com/ website, build to help develop testing skills. This project uses maven to build and run some simple scenarios. 
Technologies and models which i've used:

 - Java
 - Selenium Webdriver
 - JUnit
 - AssertJ
 - Page Object Model with Page Factory
 - Lombok
 
 ***Requirements***

Just simply install all dependencies using maven.
 
 ***How to run***

To run all features, run manually TestRunner class.

***Description***

I've decided to run test against orangehrm website, because it was specially prepared for this purpose.
Using POM (Page Object Model) with Page Factory and Lombok plug, readability of the code was very improved.
The main attention was payed to test login - logout and adding new employees funtionality. In order to do that, i've created several feature files and corresponding to them step definition - java class files. 
To open, initialize and close webdriver I've made special class - BaseUtil. It's done thanks to using Cucumber Hooks. 

 - Login.feature focus - as you can guess - on login functionality. Step definion file takes username and password. This scenatrio check wheter login is possible with correct and incorrect data. 
 - Logout.feature file does quite simple thing - just logout and check if it was successfull. 
 - ManageEmployees.feature file uses `Background` - one of the Cucumber functionalities. It's used when the same steps have to be conduct before every Scenario, thus improve code readability. 
 - AddMultipleEmployees.feature - this is the trickiest one - it uses next special Cucumber functionality called: `Scenario Outline`and corresponding to it `Examples` section. It's another way to make the code much more nicer to read. Thanks to it, i add multiple new employees to the list just in one scenario.

 ------------------------------------------------------------------------