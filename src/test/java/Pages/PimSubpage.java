package Pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

@Getter
public class PimSubpage {

    public PimSubpage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "menu_pim_viewPimModule")
    private WebElement pimModuleBtn;

    @FindBy(how = How.ID, using = "menu_pim_addEmployee")
    private WebElement addEmployeeBtn;

    @FindBy(how = How.ID, using = "menu_pim_viewEmployeeList")
    private WebElement viewEmployeeListBtn;

    @FindBy(how = How.ID, using = "firstName")
    private WebElement firstNameField;

    @FindBy(how = How.ID, using = "lastName")
    private WebElement lastNameField;

    @FindBy(how = How.ID, using = "btnSave")
    private WebElement saveNewEmployeeBtn;

    @FindBy(how = How.ID, using = "sidebar")
    private WebElement employeeSidebar;

    @FindBy(how = How.ID, using = "empsearch_employee_name_empName")
    private WebElement employeeNameSearchField;

    @FindBy(how = How.ID, using = "searchBtn")
    private WebElement searchBtn;

    @FindBy(how = How.LINK_TEXT, using = "Jan")
    private WebElement employeeName;

    @FindBy(how = How.LINK_TEXT, using = "John")
    private WebElement employeeJohnName;

    @FindBy(how = How.ID, using = "btnDelete")
    private WebElement deleteBtn;

    @FindBy(how = How.ID, using = "ohrmList_chkSelectAll")
    private WebElement deleteCheckBox;

    @FindBy(how = How.ID, using = "empsearch_id")
    private WebElement idField;

    @FindBy(how = How.XPATH, using = "//*[@id=\"dialogDeleteBtn\"]")
    private WebElement dialogDeleteOkBtn;

    @FindBy(how = How.XPATH, using = "//*[@id=\"resultTable\"]/tbody/tr/td")
    private WebElement noRecords;


    public void goToPimModule() {
        pimModuleBtn.click();
    }

    public void goToAddEmployee() {
        addEmployeeBtn.click();
    }

    public void goToViewEmployee() {
        viewEmployeeListBtn.click();
    }

    public void enterEmployeeCredentials(String firstname, String lastname) {
        firstNameField.clear();
        firstNameField.sendKeys(firstname);

        lastNameField.clear();
        lastNameField.sendKeys(lastname);
    }

    public void saveNewEmployee() {
        saveNewEmployeeBtn.click();
    }

    public boolean isNewEmployeeAdded() {
        return employeeSidebar.isDisplayed();
    }

    public boolean isNewEmployeeOnTheList() {
        return employeeName.isDisplayed();
    }

    public boolean isNewEmployeeJohnOnTheList() {
        return employeeJohnName.isDisplayed();
    }

    public void searchEmployees() {
        searchBtn.click();
    }

    public void searchEmployeeByName(String firstname) {
        employeeNameSearchField.clear();
        employeeNameSearchField.sendKeys(firstname);
    }

    public void deleteEmployees() {
        deleteCheckBox.click();
        deleteBtn.click();
    }

    public void enterEmployeeId(String id) {
        idField.clear();
        idField.sendKeys(id);
    }

    public void clickOkDeleteEmployee() {
        dialogDeleteOkBtn.click();
    }

    public boolean isNoRecordsDisplayed() {
        return noRecords.isDisplayed();
    }
}