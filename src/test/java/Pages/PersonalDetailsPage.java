package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class PersonalDetailsPage {

    public PersonalDetailsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"pdMainContainer\"]/div[1]/h1")
    private WebElement personalDetailsSection;

    @FindBy(how = How.LINK_TEXT, using = "Job")
    private WebElement jobSection;

    @FindBy(how = How.ID, using = "btnSave")
    private WebElement btnEditAndSave;

    @FindBy(how = How.ID, using = "job_emp_status")
    private WebElement selectEmploymentStatus;

    public boolean isPersonalDetailsSection() {
        return personalDetailsSection.isDisplayed();
    }

    public void goToJobSection() {
        jobSection.click();
    }

    public void clickEditOrSave() {
        btnEditAndSave.click();
    }

    public void selectByEmployeeStatus(String visibleText) {
        new Select(selectEmploymentStatus).selectByVisibleText(visibleText);
    }
}