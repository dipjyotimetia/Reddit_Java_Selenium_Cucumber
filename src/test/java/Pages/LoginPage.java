package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.NAME, using = "txtUsername")
    private WebElement usernameTxtField;

    @FindBy(how = How.NAME, using = "txtPassword")
    private WebElement passwordTxtField;

    @FindBy(how = How.ID, using = "btnLogin")
    private WebElement loginBtn;

    @FindBy(how = How.ID, using = "spanMessage")
    private WebElement invalidCredentials;

    public void enterCredentials(String username, String password){
        usernameTxtField.clear();
        usernameTxtField.sendKeys(username);

        passwordTxtField.clear();
        passwordTxtField.sendKeys(password);
    }

    public void clickLoginBtn(){
        loginBtn.click();
    }

    public boolean areCredentialsInvalid(){
        return invalidCredentials.isDisplayed();
    }

    public boolean isLoginBtnDisplayed(){
        return loginBtn.isDisplayed();
    }
}