package Pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

@Getter
public class DashboardPage {

    public DashboardPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "welcome")
    private WebElement userMenu;

    @FindBy(how = How.XPATH, using = "//*[@id=\"welcome-menu\"]/ul/li[2]/a")
    private WebElement logoutBtn;

    @FindBy(how = How.ID, using = "menu_pim_viewPimModule")
    private WebElement pimSubMenu;

    public boolean isUserLogged() {
        return userMenu.isDisplayed();
    }

    public void goToUserMenu() {
        userMenu.click();
    }

    public void logoutUser() {
        logoutBtn.click();
    }

    public void goToPIM() {
        pimSubMenu.click();
    }
}