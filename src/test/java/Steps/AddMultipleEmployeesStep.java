package Steps;

import Base.BaseUtil;
import Pages.DashboardPage;
import Pages.PersonalDetailsPage;
import Pages.PimSubpage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class AddMultipleEmployeesStep {

    private BaseUtil base;
    private DashboardPage dashboardPage;
    private PimSubpage pimSubpage;
    private PersonalDetailsPage personalDetailsPage;

    public AddMultipleEmployeesStep(BaseUtil base) {
        this.base = base;
        this.dashboardPage = new DashboardPage(base.getDriver());
        this.pimSubpage = new PimSubpage(base.getDriver());
        this.personalDetailsPage = new PersonalDetailsPage(base.getDriver());
    }

    @Given("^I am in add new employee section$")
    public void iAmInAddNewEmployeeSection() {
        base.loginAsAdmin();
        dashboardPage.goToPIM();
        pimSubpage.goToAddEmployee();
    }

    @When("^I enter ([^\"]*) and ([^\"]*) of new user$")
    public void iEnterNameAndLastnameOfNewUser(String name, String lastname) {
        pimSubpage.enterEmployeeCredentials(name, lastname);
        pimSubpage.saveNewEmployee();
        assertThat(personalDetailsPage.isPersonalDetailsSection()).isTrue();
    }

    @And("^I fill ([^\"]*) and save changes$")
    public void iFillEmploymentStatusAndJobTitle(String status) {
        personalDetailsPage.goToJobSection();
        personalDetailsPage.clickEditOrSave();
        personalDetailsPage.selectByEmployeeStatus(status);
        personalDetailsPage.clickEditOrSave();
    }

    @Then("^The new employee named ([^\"]*) should be added$")
    public void iShouldGetANewEmployeesOnTheList(String name) {
        pimSubpage.searchEmployeeByName(name);
        pimSubpage.searchEmployees();
        assertThat(pimSubpage.isNewEmployeeJohnOnTheList()).isTrue();
    }
}