package Steps;

import Base.BaseUtil;
import Pages.DashboardPage;
import Pages.LoginPage;
import Pages.PimSubpage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class EmployeeStep {
    private BaseUtil base;
    private LoginPage loginPage;
    private PimSubpage pimSubpage;

    public EmployeeStep(BaseUtil base) {
        this.base = base;
        this.loginPage = new LoginPage(base.getDriver());
        this.pimSubpage = new PimSubpage(base.getDriver());
    }

    @Given("^I log with specified credentials, username: \"([^\"]*)\" and password: \"([^\"]*)\"$")
    public void iLogWithSpecifiedCredentialsUsernameAndPassword(String username, String password) {
        base.openMainPage();
        loginPage.enterCredentials(username, password);
        loginPage.clickLoginBtn();
    }

    @When("^I go to PIM and add employee bookmark$")
    public void iGoToPIMAndAddEmployeeBookmark() {
        pimSubpage.goToPimModule();
        pimSubpage.goToAddEmployee();
    }

    @And("^I enter username \"([^\"]*)\" and last name \"([^\"]*)\"$")
    public void iEnterUsernameAndLastName(String firstname, String lastname) {
        pimSubpage.enterEmployeeCredentials(firstname, lastname);
    }

    @And("^I click save button$")
    public void iClickSaveButton() {
        pimSubpage.saveNewEmployee();
    }

    @Then("^New employee should be added$")
    public void newEmployeeShouldBeAdded() {
        assertThat(pimSubpage.isNewEmployeeAdded()).isTrue();
    }

    @When("^I go to employee list$")
    public void iGoToEmployeeList() {
        pimSubpage.goToViewEmployee();
    }

    @Then("^The new employee named \"([^\"]*)\" should be added$")
    public void theNewEmployeeNamedShouldBeAdded(String firstname) {
        pimSubpage.searchEmployeeByName(firstname);
        pimSubpage.searchEmployees();
        assertThat(pimSubpage.isNewEmployeeOnTheList()).isTrue();
    }

    @And("^Find employee with id \"([^\"]*)\"$")
    public void findEmployeeWithId(String id) {
        pimSubpage.enterEmployeeId(id);
        pimSubpage.searchEmployees();
    }

    @And("^I delete employee and refresh page$")
    public void iDeleteEmployeeAndRefreshPage() {
        pimSubpage.deleteEmployees();
        pimSubpage.clickOkDeleteEmployee();
    }

    @Then("^Employee with specified id should be removed from list$")
    public void employeeWithIdShouldBeRemovedFromList() {
        assertThat(pimSubpage.isNoRecordsDisplayed()).isTrue();
    }
}