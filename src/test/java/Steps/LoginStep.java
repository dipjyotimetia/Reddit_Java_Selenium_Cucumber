package Steps;

import Base.BaseUtil;
import Pages.DashboardPage;
import Pages.LoginPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class LoginStep extends BaseUtil {

    private BaseUtil base;
    private LoginPage loginPage;
    private DashboardPage dashboardPage;

    public LoginStep(BaseUtil base) {
        this.base = base;
        this.loginPage = new LoginPage(base.getDriver());
        this.dashboardPage = new DashboardPage(base.getDriver());
    }

    @Given("^I navigate to the login page$")
    public void iNavigateToTheLoginPage() {
        base.openMainPage();
    }

    @And("^I enter the username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
    public void iEnterTheUsernameAsAndPasswordAs(String username, String password) {
        loginPage.enterCredentials(username, password);
    }

    @When("^I click login button$")
    public void iClickLoginButton() {
        loginPage.clickLoginBtn();
    }

    @Then("^I should see the dashboard page$")
    public void iShouldSeeTheDashboardPage() {
        assertThat(dashboardPage.isUserLogged()).isTrue();
    }

    @Then("^I should get an error$")
    public void iShouldGetAnError() {
        assertThat(loginPage.areCredentialsInvalid()).isTrue();
    }

    @When("^I click logout button$")
    public void iClickLogoutButton() {
        dashboardPage.goToUserMenu();

        base.waitForPageLoad().until(ExpectedConditions.elementToBeClickable(dashboardPage.getLogoutBtn()));

        dashboardPage.logoutUser();
    }

    @Then("^I should be on the main page$")
    public void iShouldBeOnTheMainPage() {
        assertThat(loginPage.isLoginBtnDisplayed()).isTrue();
    }
}