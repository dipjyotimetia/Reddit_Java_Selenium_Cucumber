Feature: ManageEmployees
  This feature deals with admin functions such as adding new employee, in the next step
  check if it was successful and at the end delete newly created employee and check if it was successful

  Background: Login as an admin
    Given I log with specified credentials, username: "Admin" and password: "admin"
    And I go to PIM and add employee bookmark

  Scenario: Admin should be able to add new employee
    And I enter username "Jan" and last name "Kowalski"
    And I click save button
    Then New employee should be added

  Scenario: Admin should be able to find newly created employee
    When I go to employee list
    Then The new employee named "Jan" should be added

  Scenario: Admin should be able to delete newly created employee
    When I go to employee list
    And Find employee with id "0001"
    And I delete employee and refresh page
    Then Employee with specified id should be removed from list
    