Feature: AddMultipleEmployess
  This Feature deals with adding many new employees to the list

  Scenario Outline: Add 5 new employees to the list
    Given I am in add new employee section
    When I enter <name> and <lastname> of new user
    And I fill <employmentStatus> and save changes
    And I go to employee list
    Then The new employee named <name> should be added

    Examples:
      | name      | lastname | employmentStatus     |
      | John      | Nowak    | Freelance            |
      | Elisabeth | Smith    | Full-Time Contract   |
      | Patric    | Stuart   | Part-Time Contract   |
      | Miky      | Posh     | Freelance            |
      | Carol     | Sasick   | Part-Time Internship |


